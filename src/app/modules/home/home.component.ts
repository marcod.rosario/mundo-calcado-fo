import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/core/models/product.model';
import { ProductService } from 'src/app/core/services/product.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  bestSellers: Product[];
  trends: Product[];
  instagramPics: any[];
  productService: ProductService;

  getBestSellers() {
    this.productService.getMostSelled().subscribe(result => {
      this.bestSellers = result;
    });
  }

  getTrends() {
    this.productService.getTrendings().subscribe(result => {
      this.trends = result;
    });
  }

  getInstagramPics(): any[] {
    return [
      // tslint:disable-next-line: max-line-length
      { url: 'https://instagram.flis9-1.fna.fbcdn.net/vp/1f21602d0c8d62fd19c8b6e5b93369ac/5D6D9CBC/t51.2885-15/e35/54513346_407448600052056_2030828146374083868_n.jpg?_nc_ht=instagram.flis9-1.fna.fbcdn.net'},
      // tslint:disable-next-line: max-line-length
      { url: 'https://instagram.flis9-1.fna.fbcdn.net/vp/5de2fc8423addc8da6a163575f55992b/5D55BA3A/t51.2885-15/e35/44732506_1849339988521977_4371314834188973357_n.jpg?_nc_ht=instagram.flis9-1.fna.fbcdn.net'},
      // tslint:disable-next-line: max-line-length
      { url: 'https://instagram.flis9-1.fna.fbcdn.net/vp/94bf40ab35783827091845c1875a0880/5D5ADDE1/t51.2885-15/e35/54732319_335355817326535_4835784677779560237_n.jpg?_nc_ht=instagram.flis9-1.fna.fbcdn.net'},
      // tslint:disable-next-line: max-line-length
      { url: 'https://instagram.flis9-1.fna.fbcdn.net/vp/a4c9cdd89aa9bfe1c8fa957efcc7df3a/5D78A5A2/t51.2885-15/e35/44708405_102379020738828_4766015191254952149_n.jpg?_nc_ht=instagram.flis9-1.fna.fbcdn.net'},
      // tslint:disable-next-line: max-line-length
      { url: 'https://instagram.flis9-1.fna.fbcdn.net/vp/a69a427995f501d8addd1f850563a078/5D630B29/t51.2885-15/e35/44902037_525696757902790_771319797469902329_n.jpg?_nc_ht=instagram.flis9-1.fna.fbcdn.net'},
      // tslint:disable-next-line: max-line-length
      { url: 'https://instagram.flis9-1.fna.fbcdn.net/vp/4a0c1468d220362dd674a52f2399afc3/5D585E76/t51.2885-15/e35/53858031_630266520734045_5746974636100085598_n.jpg?_nc_ht=instagram.flis9-1.fna.fbcdn.net' }
    ];
  }

  constructor(productService: ProductService) {
    this.productService = productService;
   }

  ngOnInit() {
    this.getBestSellers();
    this.getTrends();
    this.instagramPics = this.getInstagramPics();
  }

}
