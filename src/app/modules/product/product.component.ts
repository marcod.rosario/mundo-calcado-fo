import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute, ParamMap, NavigationEnd } from '@angular/router';
import { Product } from 'src/app/core/models/product.model';
import { ProductService } from 'src/app/core/services/product.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {

  product: Product;
  productService: ProductService;
  quantity: number;
  relatedProducts: Product[];
  colors: string[];
  selectedColor: string;
  mySubscription: any;


  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private service: ProductService
  ) {
    // tslint:disable-next-line: only-arrow-functions
    this.router.routeReuseStrategy.shouldReuseRoute = function () {
      return false;
    };

    this.mySubscription = this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        // Trick the Router into believing it's last link wasn't previously loaded
        this.router.navigated = false;
      }
    });
  }

  increaseQuantity(elementId: string): void {
    this.quantity++;
  }

  decreaseQuantity(): void {
    if ((this.quantity - 1) >= 1) {
      this.quantity--;
    }
  }

  addCart(): void {

  }

  getProduct(id: string): Observable<Product> {
    return this.service.findProduct(id);
  }

  getRelatedProducts(id: string): Observable<Product[]> {
     return this.service.getRelatedProducts(id);
  }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');

    // Get product
    this.getProduct(id).subscribe(result => {
      this.product = result;
    });

    // Get related products
    this.service.getRelatedProducts(id).subscribe(result => {
      this.relatedProducts = result;
    });

     // Get available colors
    this.service.getAvailableColors(id).subscribe(result => {
      this.colors = result;
    });

    this.quantity = 1;
  }

  // tslint:disable-next-line: use-life-cycle-interface
  ngOnDestroy() {
    if (this.mySubscription) {
      this.mySubscription.unsubscribe();
    }
  }

}
