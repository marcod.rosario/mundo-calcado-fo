import { Component, OnInit, Input } from '@angular/core';
import { ProductService } from 'src/app/core/services/product.service';
import { Product } from 'src/app/core/models/product.model';
import { ShopService } from 'src/app/core/services/shop.service';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { FilterElement } from 'src/app/core/models/filter-element.model';

@Component({
  selector: 'app-shop',
  templateUrl: './shop.component.html',
  styleUrls: ['./shop.component.scss']
})
export class ShopComponent implements OnInit {

  //Filters
  category: string;
  brand: string;
  color: string;
  type: string;
  sort: string;

  // Filter lists
  brands: FilterElement[];
  colors: FilterElement[];
  types: FilterElement[];
  products: Product[];

  productService: ProductService;
  shopService: ShopService;
  defaultFilter: FilterElement;
  mySubscription: any;
  filterData(filter: string, type: string) {
    console.log(filter);
  }

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    shopService: ShopService) {
    this.shopService = shopService;

    // tslint:disable-next-line: only-arrow-functions
    this.router.routeReuseStrategy.shouldReuseRoute = function() {
      return false;
    };

    this.mySubscription = this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        // Trick the Router into believing it's last link wasn't previously loaded
        this.router.navigated = false;
      }
    });

  }

  ngOnInit() {

    //Sorting
    this.sort = 'sortAsc';

    this.category = this.route.snapshot.paramMap.get('category');
    // Category
    if (!this.category || this.category === '') {
      this.category = 'all';
    }

    // Brands
    this.shopService.getFilterBrands().subscribe(result => {
      this.brands = result;
    });
    this.brand = 'all';

    // Colors
    this.shopService.getFilterColors().subscribe(result => {
      this.colors = result;
    });
    this.color = 'all';

    // Types
    this.shopService.getFilterTypes().subscribe(result => {
      this.types = result;
    });
    this.type = this.route.snapshot.paramMap.get('type');
    if (!this.type || this.type === '') {
      this.type = 'all';
    }

    // TODO MR: SEND FILTERS
    this.shopService.getFilteredProducts().subscribe(result => {
      this.products = result;
    });

  }

  // tslint:disable-next-line: use-life-cycle-interface
  ngOnDestroy() {
    if (this.mySubscription) {
      this.mySubscription.unsubscribe();
    }
  }
}
