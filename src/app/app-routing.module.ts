import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './modules/home/home.component';
import { ProductComponent } from './modules/product/product.component';
import { ContactComponent } from './modules/contact/contact.component';
import { ShopComponent } from './modules/shop/shop.component';

const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'contact', component: ContactComponent },
  {
    path: 'product',
    children: [
      {
        path: ':id',
        component: ProductComponent
      }
    ]
  },
  {
    path: 'shop',
    children: [
      // {
      //   path: '',
      //   component: ShopComponent
      // },
      {
        path: ':category',
        component: ShopComponent
      },
      {
        path: ':category:type',
        component: ShopComponent
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
