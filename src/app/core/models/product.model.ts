export class Product {

    id: number;
    name: string;
    category: string;
    price: number;
    imageUrl: string;
    description: string;
    availibility: string;
    genre: string;
    composition: string;
    type: string;
    qualityCheck: string;
}
