export interface Color {

  idColor: number;
  Name: string;
  Hex: string;
  Rgb: string;

}
