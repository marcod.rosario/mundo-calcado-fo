export interface FilterElement {
    name: string;
    count: number;
}
