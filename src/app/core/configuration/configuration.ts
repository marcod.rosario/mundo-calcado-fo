export interface Configuration {
  // appSettings: AppSettings;
  service: ServiceConfig;
  // authentication: Authentication;
  // translation: Translation;
}

interface ServiceConfig {
  apiUrl: string;
}

// interface AppSettings {
//   name: string;
//   emailSupport: string;
//   version: string;
//   clientName: string;
// }

// interface Authentication {
//   // portal - signin - sso
//   type: string;
//   devMode: boolean;
//   scope: string;
//   grantType: string;
//   clientID: string;
//   sharedSecret: string;
//   secret: string;
//   responseType: string;
//   portalSettings: PortalSettings;
//   signinSettings: SigninSettings;
//   ssoSettings: SsoSettings;
// }

// interface PortalSettings {
//   url: string;
// }

// interface SigninSettings {
//   srcIdUrl: string;
//   loginUrl: string;
// }

// interface SsoSettings {
//   srcIdUrl: string;
//   redirectUrl: string;
//   postLogoutRedirectUri: string;
// }

// interface Translation {
//   switchLang: boolean;
//   defaultLang: string;
//   languages: Array<string>;
// }
