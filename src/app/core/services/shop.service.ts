import { Injectable } from '@angular/core';
import { FilterElement } from '../models/filter-element.model';
import { Product } from '../models/product.model';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ShopService {

  private serviceURL = `https://localhost:44336/api/shop`;

    // Get Brands
    getFilterBrands(): Observable<FilterElement[]> {
      const url = `${this.serviceURL}/GetFilterBrands`;
      return this.http.get<FilterElement[]>(url);
    }

    // Get Colors
    getFilterColors(): Observable<FilterElement[]> {
      const url = `${this.serviceURL}/GetFilterColors`;
      return this.http.get<FilterElement[]>(url);
    }

    // Get Types
    getFilterTypes(): Observable<FilterElement[]> {
      const url = `${this.serviceURL}/GetFilterTypes`;
      return this.http.get<FilterElement[]>(url);
    }

    getFilteredProducts(): Observable<Product[]> {
      const url = `https://localhost:44336/api/product/GetTrends`;
      return this.http.get<Product[]>(url);
    }

  constructor(
    private http: HttpClient,
  ) {

  }
}
