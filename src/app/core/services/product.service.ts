import { Injectable } from '@angular/core';
import { Product } from '../models/product.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ConfigurationService } from '../configuration/configuration.service';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  // private serviceURL = `${this.configurationService.configuration.service.apiUrl}/product`;
  private serviceURL = `https://localhost:44336/api/product`;

  // Get Trends
  getTrendings(): Observable<Product[]> {
    const url = `${this.serviceURL}/GetTrends`;
    return this.http.get<Product[]>(url);
  }

  // Get More selled
  getMostSelled(): Observable<Product[]> {
    const url = `${this.serviceURL}/GetMostSelled`;
    return this.http.get<Product[]>(url);
  }

  // Find Product
  findProduct(id: string): Observable<Product> {
    const url = `${this.serviceURL}/GetProduct/${id}`;
    return this.http.get<Product>(url);
  }

  getRelatedProducts(id: string): Observable<Product[]> {
    const url = `${this.serviceURL}/GetRelatedProducts/${id}`;
    return this.http.get<Product[]>(url);
  }

  getAvailableColors(id: string): Observable<string[]> {
    const url = `${this.serviceURL}/GetAvailableColors/${id}`;
    return this.http.get<string[]>(url);
  }

  constructor(
    private http: HttpClient,
    private configurationService: ConfigurationService) {
  }
}
